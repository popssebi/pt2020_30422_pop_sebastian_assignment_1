package control;

import view.Interface;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Operations {

    private String finalResult = "" ;
    private List<Integer> resultedPolynomial = new ArrayList<>();
    private int sizeOfResultedPolynomial;

    public void sum( List<Integer> firstPolynomial , List<Integer> secondPolynomial , int sizeOfFirstPolynomial , int sizeOfSecondPolynomial ){

        if( sizeOfFirstPolynomial >= sizeOfSecondPolynomial ){                                  // we check which polynomial is bigger
            for( int w = 0 ; w < sizeOfFirstPolynomial ; w++ ){resultedPolynomial.add(0);}      // in order to set the resulted polynomial size as the greater one
            sizeOfResultedPolynomial = sizeOfFirstPolynomial;
        }
        else{
            for( int w = 0 ; w < sizeOfSecondPolynomial ; w++ ){resultedPolynomial.add(0);}
            sizeOfResultedPolynomial = sizeOfSecondPolynomial;
        }

        if( sizeOfFirstPolynomial == sizeOfSecondPolynomial ){
            for( int i = 0 ; i < sizeOfResultedPolynomial ; i++ ){
                resultedPolynomial.set( i , firstPolynomial.get(i)+secondPolynomial.get(i));    // addition of the coefficients
            }

        }
        else{
            if( sizeOfSecondPolynomial < sizeOfFirstPolynomial ){
                int i;
                for( i = 0 ; i < sizeOfSecondPolynomial ; i++ ){
                    resultedPolynomial.set( i , firstPolynomial.get(i)+secondPolynomial.get(i) );   // addition of the coefficients
                }
                for( int j = i ; j < sizeOfFirstPolynomial ; j++ ){
                    resultedPolynomial.set( j , firstPolynomial.get(j));    // filling the rest of the terms unchanged because they do not have a term to be added to
                }
            }
            else{
                if( sizeOfFirstPolynomial < sizeOfSecondPolynomial ){
                    int i;
                    for( i = 0 ; i < sizeOfFirstPolynomial ; i++ ){
                        resultedPolynomial.set( i , firstPolynomial.get(i)+secondPolynomial.get(i));
                    }
                    for( int j = i ; j < sizeOfSecondPolynomial ; j++ ){
                        resultedPolynomial.set( j , secondPolynomial.get(j) );
                    }
                }
            }
        }


        String[] auxResult = new String[sizeOfResultedPolynomial];      // auxResult is used in order to start concatenating the resulted monomials in the final polynomial
        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial.get(i) >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial.get(i)));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial.get(i));
        }

        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);     // final polynomial is being constructed
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);
    }

    public void difference( List<Integer> firstPolynomial , List<Integer> secondPolynomial , int sizeOfFirstPolynomial , int sizeOfSecondPolynomial ){

        if(sizeOfFirstPolynomial >= sizeOfSecondPolynomial){
            for( int w = 0 ; w < sizeOfFirstPolynomial ; w++ ){resultedPolynomial.add(0);}
            sizeOfResultedPolynomial = sizeOfFirstPolynomial;
        }
        else{
            for( int w = 0 ; w < sizeOfSecondPolynomial ; w++ ){resultedPolynomial.add(0);}
            sizeOfResultedPolynomial = sizeOfSecondPolynomial;
        }
        if( sizeOfFirstPolynomial == sizeOfSecondPolynomial) {
            for (int i = 0; i < sizeOfResultedPolynomial; i++) {
                resultedPolynomial.set( i , firstPolynomial.get(i) - secondPolynomial.get(i));
            }
        }
        else{
            if( sizeOfSecondPolynomial < sizeOfFirstPolynomial ){
                int i;
                for(i = 0 ; i < sizeOfSecondPolynomial ; i++){
                    resultedPolynomial.set( i , firstPolynomial.get(i) - secondPolynomial.get(i));
                }
                for(int j = i ; j < sizeOfFirstPolynomial ; j++){
                    resultedPolynomial.set( j , firstPolynomial.get(j));
                }
            }
            else{
                if( sizeOfFirstPolynomial < sizeOfSecondPolynomial ){
                    int i;
                    for( i = 0 ; i < sizeOfFirstPolynomial ; i++){
                        resultedPolynomial.set( i , firstPolynomial.get(i) - secondPolynomial.get(i));
                    }
                    for(int j = i ; j < sizeOfSecondPolynomial ; j++){
                        resultedPolynomial.set( j , secondPolynomial.get(j));
                    }
                }
            }
        }
        String[] auxResult = new String[sizeOfResultedPolynomial];
        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial.get(i) >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial.get(i)));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial.get(i));
        }
        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);
    }

    public void multiply( List<Integer> firstPolynomial , List<Integer> secondPolynomial , int sizeOfFirstPolynomial , int sizeOfSecondPolynomial ){
        sizeOfResultedPolynomial = sizeOfFirstPolynomial + sizeOfSecondPolynomial - 1;
        for( int w = 0 ; w < sizeOfResultedPolynomial ; w++ ){resultedPolynomial.add(0);}

        for(int i = 0 ; i < sizeOfFirstPolynomial ; i++){
            for(int j = 0 ; j < sizeOfSecondPolynomial ; j++){
                int k = i+j;
                resultedPolynomial.set( k , resultedPolynomial.get(k) + (firstPolynomial.get(i)*secondPolynomial.get(j)) );
            }
        }

        String[] auxResult = new String[sizeOfResultedPolynomial];

        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial.get(i) >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial.get(i)));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial.get(i));
        }

        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);


    }

}
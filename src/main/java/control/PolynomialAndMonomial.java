package control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolynomialAndMonomial {

   private List<Integer> firstPolynomial = new ArrayList<>();
   private List<Integer> secondPolynomial = new ArrayList<>();
   private int sizeOfFirstPolynomial, sizeOfSecondPolynomial;

   public void getFirstPolynomial(String auxString){

       char firstChar;
       firstChar = auxString.charAt(0);         // firstChar is used to store the first char of the polynomial added by the user

       String replacedAuxString = auxString.replace("-","+-");          // in the given polynomial we replace '-' with '+-' in order to correctly split the polynomial into monomials
       List<String> splitStringList = new ArrayList<String>(Arrays.asList(replacedAuxString.split("[+]")));     // we split the polynomial at the '+' character
       sizeOfFirstPolynomial = splitStringList.size();

       if( firstChar == '-' ){              // we check if the first monomial of the polynomial is a negative one, if it is, we need to shift left in order to recover the correct sign of the monomial
           for( int j = 0 ; j < sizeOfFirstPolynomial - 1 ; j++ ){          // example: -2x^3+5x^2-12x^1+6x^0 => replaceAuxString => +-2x^3+5x^2+-12x^1+6x^0 => splitString => [+-2x^3, 5x^2, -12x^1, 6x^0]
               splitStringList.set(   j  ,  splitStringList.get(j+1)   );   // so we need to fix the first monomial by having -2x^3 instead of +-2x^3
           }
           sizeOfFirstPolynomial--;
       }

       for( int w = 0 ; w < sizeOfFirstPolynomial ; w++ ){
           firstPolynomial.add(0);
       }

       for( int z = 0 ; z < sizeOfFirstPolynomial ; z++ ){

           List<String> aux = new ArrayList<>(Arrays.asList(splitStringList.get(z).split("[*x^]")));    // after having the monomials we split every monomial in order to remain with only the coefficient and the power
           int coefficient = Integer.valueOf(aux.get(0));                                                   // we convert the coefficient from String to Integer
           int power = sizeOfFirstPolynomial -z-1;                                                          // to get the power we take the size of the polynomial and subtract the current index and 1 out of it
           firstPolynomial.set(power,coefficient);

       }
   }

   public void getSecondPolynomial(String auxString){

       char firstChar;
       firstChar = auxString.charAt(0);

       String replacedAuxString = auxString.replace("-","+-");
       List<String> splitStringList = new ArrayList<String>(Arrays.asList(replacedAuxString.split("[+]")));
       sizeOfSecondPolynomial = splitStringList.size();

       if( firstChar == '-' ){
           for( int j = 0 ; j < sizeOfSecondPolynomial - 1 ; j++ ){
               splitStringList.set(   j  ,  splitStringList.get(j+1)   );
           }
           sizeOfSecondPolynomial--;
       }

       for( int w = 0 ; w < sizeOfSecondPolynomial ; w++ ){
           secondPolynomial.add(0);
       }

       for( int z = 0 ; z < sizeOfSecondPolynomial ; z++ ){

           List<String> aux = new ArrayList<>(Arrays.asList(splitStringList.get(z).split("[*x^]")));
           int coefficient = Integer.valueOf(aux.get(0));
           int power = sizeOfSecondPolynomial -z-1;
           secondPolynomial.set(power,coefficient);

       }
   }

    public void getThirdPolynomial(int pickOperation){      // the operation method on polynomials is called depending on the operation picked by the user

        Operations accessOperations = new Operations();

        switch (pickOperation) {
            case 1: accessOperations.sum(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
                break;
            case 2: accessOperations.difference(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
                break;
            case 3: accessOperations.multiply(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
                /*break;
            case 4: accessOperations.derivative(firstPolynomial, sizeOfFirstPolynomial);*/
        }

    }



}
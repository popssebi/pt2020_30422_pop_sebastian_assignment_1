package main;
import view.Interface;

public class Main {

    public static void main(String[] args) {

        Interface accessInterface = new Interface();    // accesses the layout method from the Interface class
        accessInterface.layout();                       // in in order to start the program with the GUI

    }
}
